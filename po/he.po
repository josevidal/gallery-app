# Hebrew translation for gallery-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the gallery-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: gallery-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-11-22 21:06+0000\n"
"PO-Revision-Date: 2020-12-24 08:57+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew <https://translate.ubports.com/projects/ubports/"
"gallery-app/he/>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-05 07:16+0000\n"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:43
#: rc/qml/AlbumEditor/AlbumEditor.qml:36
msgid "Edit album"
msgstr "עריכת האלבום"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:50
#: rc/qml/AlbumViewer/AlbumViewer.qml:378 rc/qml/Components/DeleteDialog.qml:32
#: rc/qml/EventsOverview.qml:139 rc/qml/MediaViewer/MediaViewer.qml:350
#: rc/qml/PhotosOverview.qml:195 rc/qml/Utility/EditingHUD.qml:66
msgid "Delete"
msgstr "מחיקה"

#: rc/qml/AlbumEditor/AlbumEditor.qml:39 rc/qml/Components/MediaSelector.qml:93
#: rc/qml/EventsOverview.qml:161 rc/qml/MediaViewer/MediaViewer.qml:278
#: rc/qml/PhotosOverview.qml:218 rc/qml/PickerScreen.qml:285
msgid "Cancel"
msgstr "ביטול"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:113
msgid "Default"
msgstr "ברירת מחדל"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:114
msgid "Blue"
msgstr "כחול"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:115
msgid "Green"
msgstr "ירוק"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:116
msgid "Pattern"
msgstr "תבנית"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:117
msgid "Red"
msgstr "אדום"

#: rc/qml/AlbumViewer/AlbumViewer.qml:107 rc/qml/MainScreen.qml:62
msgid "Album"
msgstr "אלבום"

#: rc/qml/AlbumViewer/AlbumViewer.qml:372
#: rc/qml/MediaViewer/MediaViewer.qml:340
msgid "Add to album"
msgstr "הוספה לאלבום"

#: rc/qml/AlbumsOverview.qml:229 rc/qml/Components/PopupAlbumPicker.qml:91
msgid "Add new album"
msgstr "הוספת אלבום חדש"

#: rc/qml/AlbumsOverview.qml:233 rc/qml/Components/PopupAlbumPicker.qml:101
msgid "New Photo Album"
msgstr "אלבום תמונות חדש"

#: rc/qml/AlbumsOverview.qml:234 rc/qml/Components/PopupAlbumPicker.qml:102
msgid "Subtitle"
msgstr "כותרת משנה"

#: rc/qml/AlbumsOverview.qml:241 rc/qml/EventsOverview.qml:116
#: rc/qml/PhotosOverview.qml:173
msgid "Camera"
msgstr "מצלמה"

#: rc/qml/Components/DeleteDialog.qml:36 rc/qml/MediaViewer/MediaViewer.qml:226
msgid "Yes"
msgstr "כן"

#: rc/qml/Components/DeleteDialog.qml:45 rc/qml/MediaViewer/MediaViewer.qml:237
msgid "No"
msgstr "לא"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:51
msgid "Delete album"
msgstr "מחיקת האלבום"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:60
msgid "Delete album AND contents"
msgstr "מחק אלבום ותוכן"

#: rc/qml/Components/EventCard.qml:59
msgid "MMM yyyy"
msgstr "MMM yyyy"

#: rc/qml/Components/EventCard.qml:73
msgid "dd"
msgstr "dd"

#: rc/qml/Components/MediaSelector.qml:59
#: rc/qml/Components/MediaSelector.qml:81
msgid "Add to Album"
msgstr "הוספה לאלבום"

#: rc/qml/Components/PopupAlbumPicker.qml:162 rc/qml/Utility/EditingHUD.qml:77
msgid "Add Photo to Album"
msgstr "הוספת תמונה לאלבום"

#: rc/qml/Components/UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "לא ניתן לשתף"

#: rc/qml/Components/UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "לא ניתן לשתף תמונות ווידאו באותו הזמן"

#: rc/qml/Components/UnableShareDialog.qml:30
msgid "Ok"
msgstr "אישור"

#: rc/qml/EventsOverview.qml:80 rc/qml/PhotosOverview.qml:87
#, qt-format
msgid "Delete %1 photo"
msgid_plural "Delete %1 photos"
msgstr[0] "מחיקת %1 תמונה"
msgstr[1] "מחיקת %1 תמונות"

#: rc/qml/EventsOverview.qml:86 rc/qml/PhotosOverview.qml:93
#, qt-format
msgid "Delete %1 video"
msgid_plural "Delete %1 videos"
msgstr[0] "מחיקת %1 סרטון"
msgstr[1] "מחיקת %1 סרטונים"

#: rc/qml/EventsOverview.qml:92 rc/qml/PhotosOverview.qml:99
#, qt-format
msgid "Delete %1 media file"
msgid_plural "Delete %1 media files"
msgstr[0] "מחק את קובץ המדיה %1"
msgstr[1] "מחק %1 קבצי מדיה"

#: rc/qml/EventsOverview.qml:109 rc/qml/MainScreen.qml:199
#: rc/qml/MainScreen.qml:251 rc/qml/PhotosOverview.qml:166
#: rc/qml/PickerScreen.qml:192 rc/qml/PickerScreen.qml:239
msgid "Select"
msgstr "בחר"

#: rc/qml/EventsOverview.qml:128 rc/qml/PhotosOverview.qml:185
#: rc/qml/Utility/EditingHUD.qml:76
msgid "Add"
msgstr "הוספה"

#: rc/qml/EventsOverview.qml:146 rc/qml/MediaViewer/MediaViewer.qml:361
#: rc/qml/PhotosOverview.qml:202 rc/qml/Utility/EditingHUD.qml:71
msgid "Share"
msgstr "שיתוף"

#: rc/qml/EventsOverview.qml:195 rc/qml/MediaViewer/MediaViewer.qml:182
#: rc/qml/PhotosOverview.qml:241
msgid "Share to"
msgstr "שיתוף אל"

#: rc/qml/LoadingScreen.qml:39
msgid "Loading…"
msgstr "בטעינה…"

#: rc/qml/MainScreen.qml:148
msgid "Albums"
msgstr "אלבומים"

#: rc/qml/MainScreen.qml:162 rc/qml/MainScreen.qml:201
#: rc/qml/PickerScreen.qml:154
msgid "Events"
msgstr "אירועים"

#: rc/qml/MainScreen.qml:210 rc/qml/MainScreen.qml:253
#: rc/qml/PickerScreen.qml:203
msgid "Photos"
msgstr "תמונות"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a photo"
msgstr "מחיקת תמונה"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a video"
msgstr "מחיקת סרטון"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a photo from album"
msgstr "הסרת תמונה מאלבום"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a video from album"
msgstr "הסרת סרטון מאלבום"

#: rc/qml/MediaViewer/MediaViewer.qml:257
msgid "Remove from Album"
msgstr "הסרה מאלבום"

#: rc/qml/MediaViewer/MediaViewer.qml:268
msgid "Remove from Album and Delete"
msgstr "הסרה מאלבום ומחיקה"

#: rc/qml/MediaViewer/MediaViewer.qml:315
msgid "Edit"
msgstr "עריכה"

#: rc/qml/MediaViewer/MediaViewer.qml:371
msgid "Info"
msgstr "מידע"

#: rc/qml/MediaViewer/MediaViewer.qml:394
msgid "Informations"
msgstr "מידע"

#: rc/qml/MediaViewer/MediaViewer.qml:397
msgid "Media type: "
msgstr "סוג המדיה: "

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "photo"
msgstr "תמונה"

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "video"
msgstr "וידאו"

#: rc/qml/MediaViewer/MediaViewer.qml:399
msgid "Media name: "
msgstr "שם מדיה: "

#: rc/qml/MediaViewer/MediaViewer.qml:400
msgid "Date: "
msgstr "תאריך: "

#: rc/qml/MediaViewer/MediaViewer.qml:401
msgid "Time: "
msgstr "זמן: "

#: rc/qml/MediaViewer/MediaViewer.qml:407
msgid "ok"
msgstr "בסדר"

#: rc/qml/MediaViewer/PhotoEditorPage.qml:26
msgid "Edit Photo"
msgstr "עריכת תמונה"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:46
#: desktop/gallery-app.desktop.in.in.h:1
msgid "Gallery"
msgstr "גלריה"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:183
msgid "Toggle Selection"
msgstr "הפיכת בחירה"

#: rc/qml/MediaViewer/SingleMediaViewer.qml:241
msgid "An error has occurred attempting to load media"
msgstr "אירעה שגיאה בעת הניסיון לטעינת המדיה"

#: rc/qml/PhotosOverview.qml:116 rc/qml/PhotosOverview.qml:159
msgid "Grid Size"
msgstr "גודל רשת"

#: rc/qml/PhotosOverview.qml:117
msgid "Select the grid size in gu units between 8 and 20 (default is 12)"
msgstr "בחר את גודל הרשת ביחידות gu בין 8 ל -20 (ברירת המחדל היא 12)"

#: rc/qml/PhotosOverview.qml:132
msgid "Finished"
msgstr "הסתיים"

#: rc/qml/PickerScreen.qml:291
msgid "Pick"
msgstr "בחירה"

#: rc/qml/Utility/EditingHUD.qml:67
msgid "Trash;Erase"
msgstr "אשפה;מחיקה;השלכה"

#: rc/qml/Utility/EditingHUD.qml:72
msgid "Post;Upload;Attach"
msgstr "פרסום;העלאה;צירוף"

#: rc/qml/Utility/EditingHUD.qml:81
msgid "Undo"
msgstr "החזרה"

#: rc/qml/Utility/EditingHUD.qml:82
msgid "Cancel Action;Backstep"
msgstr "ביטול הפעולה;צעד אחורה"

#: rc/qml/Utility/EditingHUD.qml:86
msgid "Redo"
msgstr "ביצוע חוזר"

#: rc/qml/Utility/EditingHUD.qml:87
msgid "Reapply;Make Again"
msgstr "להחיל מחדש;לבצע שוב"

#: rc/qml/Utility/EditingHUD.qml:91
msgid "Auto Enhance"
msgstr "שיפור אוטומטי"

#: rc/qml/Utility/EditingHUD.qml:92
msgid "Adjust the image automatically"
msgstr "התאמת התמונה אוטומטית"

#: rc/qml/Utility/EditingHUD.qml:93
msgid "Automatically Adjust Photo"
msgstr "התאמת התמונה אוטומטית"

#: rc/qml/Utility/EditingHUD.qml:98
msgid "Rotate"
msgstr "סיבוב"

#: rc/qml/Utility/EditingHUD.qml:99
msgid "Turn Clockwise"
msgstr "סיבוב עם כיוון השעון"

#: rc/qml/Utility/EditingHUD.qml:100
msgid "Rotate the image clockwise"
msgstr "סיבוב התמונה עם כיוון השעון"

#: rc/qml/Utility/EditingHUD.qml:105
msgid "Crop"
msgstr "חיתוך"

#: rc/qml/Utility/EditingHUD.qml:106
msgid "Trim;Cut"
msgstr "גזירה;קיצוץ"

#: rc/qml/Utility/EditingHUD.qml:107
msgid "Crop the image"
msgstr "חיתוך התמונה"

#: rc/qml/Utility/EditingHUD.qml:112
msgid "Revert to Original"
msgstr "שחזור למקור"

#: rc/qml/Utility/EditingHUD.qml:113
msgid "Discard Changes"
msgstr "התעלמות מהשינויים"

#: rc/qml/Utility/EditingHUD.qml:114
msgid "Discard all changes"
msgstr "התעלמות מכל השינויים"

#: rc/qml/Utility/EditingHUD.qml:118
msgid "Exposure"
msgstr "חשיפה"

#: rc/qml/Utility/EditingHUD.qml:119
msgid "Adjust the exposure"
msgstr "הגדרת החשיפה"

#: rc/qml/Utility/EditingHUD.qml:120
msgid "Underexposed;Overexposed"
msgstr "חשיפת יתר;חשיפה מופחתת"

#: rc/qml/Utility/EditingHUD.qml:121 rc/qml/Utility/EditingHUD.qml:162
msgid "Confirm"
msgstr "אישור"

#: rc/qml/Utility/EditingHUD.qml:125
msgid "Compensation"
msgstr "פיצוי"

#: rc/qml/Utility/EditingHUD.qml:159
msgid "Color Balance"
msgstr "‏איזון צבע"

#: rc/qml/Utility/EditingHUD.qml:160
msgid "Adjust color balance"
msgstr "התאמת איזון הצבע"

#: rc/qml/Utility/EditingHUD.qml:161
msgid "Saturation;Hue"
msgstr "רוויה;גוון"

#: rc/qml/Utility/EditingHUD.qml:166
msgid "Brightness"
msgstr "בהירות"

#: rc/qml/Utility/EditingHUD.qml:177
msgid "Contrast"
msgstr "ניגודיות"

#: rc/qml/Utility/EditingHUD.qml:188
msgid "Saturation"
msgstr "רוויה"

#: rc/qml/Utility/EditingHUD.qml:199
msgid "Hue"
msgstr "גוון"

#: desktop/gallery-app.desktop.in.in.h:2
msgid "Ubuntu Photo Viewer"
msgstr "מציג התמונות של אובונטו"

#: desktop/gallery-app.desktop.in.in.h:3
msgid "Browse your photographs"
msgstr "עיון בין הצילומים שלך"

#: desktop/gallery-app.desktop.in.in.h:4
msgid "Photographs;Pictures;Albums"
msgstr "תמונות;צילומים;אלבומים"

#~ msgid "Delete 1 photo"
#~ msgstr "מחיקת תמונה אחת"

#~ msgid "Delete 1 video"
#~ msgstr "מחיקת סרטון אחד"

#~ msgid "Delete %1 photos and 1 video"
#~ msgstr "מחיקת %1 תמונות וסרטון אחד"

#~ msgid "Delete 1 photo and %1 videos"
#~ msgstr "מחיקת תמונה אחת ו־%1 סרטונים"

#~ msgid "Delete 1 photo and 1 video"
#~ msgstr "מחיקת תמונה אחת וסרטון אחד"

#~ msgid "Delete %1 photos and %2 videos"
#~ msgstr "מחיקת %1 תמונות ו־%2 סרטונים"
