# Gaelic; Scottish translation for gallery-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the gallery-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
# GunChleoc <fios@foramnagaidhlig.net>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: gallery-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-11-22 21:06+0000\n"
"PO-Revision-Date: 2016-04-07 08:49+0000\n"
"Last-Translator: Akerbeltz <fios@akerbeltz.org>\n"
"Language-Team: Fòram na Gàidhlig\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2017-04-05 07:16+0000\n"
"X-Generator: Launchpad (build 18335)\n"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:43
#: rc/qml/AlbumEditor/AlbumEditor.qml:36
msgid "Edit album"
msgstr "Deasaich an t-albam"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:50
#: rc/qml/AlbumViewer/AlbumViewer.qml:378 rc/qml/Components/DeleteDialog.qml:32
#: rc/qml/EventsOverview.qml:139 rc/qml/MediaViewer/MediaViewer.qml:350
#: rc/qml/PhotosOverview.qml:195 rc/qml/Utility/EditingHUD.qml:66
msgid "Delete"
msgstr "Sguab às"

#: rc/qml/AlbumEditor/AlbumEditor.qml:39 rc/qml/Components/MediaSelector.qml:93
#: rc/qml/EventsOverview.qml:161 rc/qml/MediaViewer/MediaViewer.qml:278
#: rc/qml/PhotosOverview.qml:218 rc/qml/PickerScreen.qml:285
msgid "Cancel"
msgstr "Sguir dheth"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:113
msgid "Default"
msgstr "Bun-roghainn"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:114
msgid "Blue"
msgstr "Gorm"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:115
msgid "Green"
msgstr "Uaine"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:116
msgid "Pattern"
msgstr "Pàtran"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:117
msgid "Red"
msgstr "Dearg"

#: rc/qml/AlbumViewer/AlbumViewer.qml:107 rc/qml/MainScreen.qml:62
msgid "Album"
msgstr "Albam"

#: rc/qml/AlbumViewer/AlbumViewer.qml:372
#: rc/qml/MediaViewer/MediaViewer.qml:340
msgid "Add to album"
msgstr "Cuir ris an albam"

#: rc/qml/AlbumsOverview.qml:229 rc/qml/Components/PopupAlbumPicker.qml:91
msgid "Add new album"
msgstr "Cuir albam ùr ris"

#: rc/qml/AlbumsOverview.qml:233 rc/qml/Components/PopupAlbumPicker.qml:101
msgid "New Photo Album"
msgstr "Albam dhealbhan ùr"

#: rc/qml/AlbumsOverview.qml:234 rc/qml/Components/PopupAlbumPicker.qml:102
msgid "Subtitle"
msgstr "Fo-thiotal"

#: rc/qml/AlbumsOverview.qml:241 rc/qml/EventsOverview.qml:116
#: rc/qml/PhotosOverview.qml:173
msgid "Camera"
msgstr "Camara"

#: rc/qml/Components/DeleteDialog.qml:36 rc/qml/MediaViewer/MediaViewer.qml:226
msgid "Yes"
msgstr "Tha"

#: rc/qml/Components/DeleteDialog.qml:45 rc/qml/MediaViewer/MediaViewer.qml:237
msgid "No"
msgstr "Chan eil"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:51
msgid "Delete album"
msgstr "Sguab às an t-albam"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:60
#, fuzzy
msgid "Delete album AND contents"
msgstr "Sguab às an t-albam ⁊ an t-susbaint"

#: rc/qml/Components/EventCard.qml:59
msgid "MMM yyyy"
msgstr "MMM yyyy"

#: rc/qml/Components/EventCard.qml:73
msgid "dd"
msgstr "dd"

#: rc/qml/Components/MediaSelector.qml:59
#: rc/qml/Components/MediaSelector.qml:81
msgid "Add to Album"
msgstr "Cuir ris an albam"

#: rc/qml/Components/PopupAlbumPicker.qml:162 rc/qml/Utility/EditingHUD.qml:77
msgid "Add Photo to Album"
msgstr "Cuir an dealbh ris an albam"

#: rc/qml/Components/UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "Cha ghabh a cho-roinneadh"

#: rc/qml/Components/UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "Cha ghabh dealbhan is videothan a cho-roinneadh aig an aon àm"

#: rc/qml/Components/UnableShareDialog.qml:30
msgid "Ok"
msgstr "Ceart ma-thà"

#: rc/qml/EventsOverview.qml:80 rc/qml/PhotosOverview.qml:87
#, fuzzy, qt-format
msgid "Delete %1 photo"
msgid_plural "Delete %1 photos"
msgstr[0] "Sguab às %1 dealbh(an)"
msgstr[1] "Sguab às %1 dealbh(an)"

#: rc/qml/EventsOverview.qml:86 rc/qml/PhotosOverview.qml:93
#, fuzzy, qt-format
msgid "Delete %1 video"
msgid_plural "Delete %1 videos"
msgstr[0] "Sguab às %1 video(than)"
msgstr[1] "Sguab às %1 video(than)"

#: rc/qml/EventsOverview.qml:92 rc/qml/PhotosOverview.qml:99
#, fuzzy, qt-format
msgid "Delete %1 media file"
msgid_plural "Delete %1 media files"
msgstr[0] "Sguab às %1 video(than)"
msgstr[1] "Sguab às %1 video(than)"

#: rc/qml/EventsOverview.qml:109 rc/qml/MainScreen.qml:199
#: rc/qml/MainScreen.qml:251 rc/qml/PhotosOverview.qml:166
#: rc/qml/PickerScreen.qml:192 rc/qml/PickerScreen.qml:239
msgid "Select"
msgstr "Tagh"

#: rc/qml/EventsOverview.qml:128 rc/qml/PhotosOverview.qml:185
#: rc/qml/Utility/EditingHUD.qml:76
msgid "Add"
msgstr "Cuir ris"

#: rc/qml/EventsOverview.qml:146 rc/qml/MediaViewer/MediaViewer.qml:361
#: rc/qml/PhotosOverview.qml:202 rc/qml/Utility/EditingHUD.qml:71
msgid "Share"
msgstr "Co-roinn"

#: rc/qml/EventsOverview.qml:195 rc/qml/MediaViewer/MediaViewer.qml:182
#: rc/qml/PhotosOverview.qml:241
msgid "Share to"
msgstr "Co-roinn air"

#: rc/qml/LoadingScreen.qml:39
msgid "Loading…"
msgstr "'Ga luchdadh…"

#: rc/qml/MainScreen.qml:148
msgid "Albums"
msgstr "Albaman"

#: rc/qml/MainScreen.qml:162 rc/qml/MainScreen.qml:201
#: rc/qml/PickerScreen.qml:154
msgid "Events"
msgstr "Tachartasan"

#: rc/qml/MainScreen.qml:210 rc/qml/MainScreen.qml:253
#: rc/qml/PickerScreen.qml:203
msgid "Photos"
msgstr "Dealbhan"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a photo"
msgstr "Sguab às dealbh"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a video"
msgstr "Sguab às video"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a photo from album"
msgstr "Thoir dealbh air falbh on albam"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a video from album"
msgstr "Sguab às video on albam"

#: rc/qml/MediaViewer/MediaViewer.qml:257
msgid "Remove from Album"
msgstr "Thoir air falbh on albam"

#: rc/qml/MediaViewer/MediaViewer.qml:268
msgid "Remove from Album and Delete"
msgstr "Thoir air falbh on albam 's sguab às"

#: rc/qml/MediaViewer/MediaViewer.qml:315
msgid "Edit"
msgstr "Deasaich"

#: rc/qml/MediaViewer/MediaViewer.qml:371
msgid "Info"
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:394
msgid "Informations"
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:397
msgid "Media type: "
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:398
#, fuzzy
msgid "photo"
msgstr "Dealbhan"

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "video"
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:399
msgid "Media name: "
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:400
msgid "Date: "
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:401
msgid "Time: "
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:407
msgid "ok"
msgstr ""

#: rc/qml/MediaViewer/PhotoEditorPage.qml:26
msgid "Edit Photo"
msgstr "Deasaich an dealbh"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:46
#: desktop/gallery-app.desktop.in.in.h:1
msgid "Gallery"
msgstr "Gailearaidh"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:183
msgid "Toggle Selection"
msgstr "Toglaich na thagh thu"

#: rc/qml/MediaViewer/SingleMediaViewer.qml:241
msgid "An error has occurred attempting to load media"
msgstr ""
"Thachair mearachd fhad 's a bha sinn a' feuchainn ri meadhan a luchdadh"

#: rc/qml/PhotosOverview.qml:116 rc/qml/PhotosOverview.qml:159
msgid "Grid Size"
msgstr ""

#: rc/qml/PhotosOverview.qml:117
msgid "Select the grid size in gu units between 8 and 20 (default is 12)"
msgstr ""

#: rc/qml/PhotosOverview.qml:132
msgid "Finished"
msgstr ""

#: rc/qml/PickerScreen.qml:291
msgid "Pick"
msgstr "Tagh"

#: rc/qml/Utility/EditingHUD.qml:67
msgid "Trash;Erase"
msgstr "Trash;Erase;sgudal;sguab às"

#: rc/qml/Utility/EditingHUD.qml:72
msgid "Post;Upload;Attach"
msgstr "Post;Upload;Attach;postaich;luchdaich suas;ceangail"

#: rc/qml/Utility/EditingHUD.qml:81
msgid "Undo"
msgstr "Neo-dhèan"

#: rc/qml/Utility/EditingHUD.qml:82
msgid "Cancel Action;Backstep"
msgstr "Cancel Action;Backstep;sguir dheth"

#: rc/qml/Utility/EditingHUD.qml:86
msgid "Redo"
msgstr "Ath-dhèan"

#: rc/qml/Utility/EditingHUD.qml:87
msgid "Reapply;Make Again"
msgstr "Reapply;Make Again;cuir an sàs a-rithist;dèan a-rithist"

#: rc/qml/Utility/EditingHUD.qml:91
msgid "Auto Enhance"
msgstr "Fèin-mheudaich"

#: rc/qml/Utility/EditingHUD.qml:92
msgid "Adjust the image automatically"
msgstr "Cuir air gleus an dealbh gu fèin-obrachail"

#: rc/qml/Utility/EditingHUD.qml:93
msgid "Automatically Adjust Photo"
msgstr "Cuir air gleus an dealbh gu fèin-obrachail"

#: rc/qml/Utility/EditingHUD.qml:98
msgid "Rotate"
msgstr "Cuairtich"

#: rc/qml/Utility/EditingHUD.qml:99
msgid "Turn Clockwise"
msgstr "Cuirtich deiseil"

#: rc/qml/Utility/EditingHUD.qml:100
msgid "Rotate the image clockwise"
msgstr "Cuirtich an dealbh deiseil"

#: rc/qml/Utility/EditingHUD.qml:105
msgid "Crop"
msgstr "Bearr"

#: rc/qml/Utility/EditingHUD.qml:106
msgid "Trim;Cut"
msgstr "Trim;Cut;bearr;gearr"

#: rc/qml/Utility/EditingHUD.qml:107
msgid "Crop the image"
msgstr "Bearr an dealbh"

#: rc/qml/Utility/EditingHUD.qml:112
msgid "Revert to Original"
msgstr "Till an tionndadh tùsail"

#: rc/qml/Utility/EditingHUD.qml:113
msgid "Discard Changes"
msgstr "Tilg air falbh na h-atharraichean"

#: rc/qml/Utility/EditingHUD.qml:114
msgid "Discard all changes"
msgstr "Tilg air falbh gach atharrachadh"

#: rc/qml/Utility/EditingHUD.qml:118
msgid "Exposure"
msgstr "Deisearas"

#: rc/qml/Utility/EditingHUD.qml:119
msgid "Adjust the exposure"
msgstr "Cuir an deisearas air gleus"

#: rc/qml/Utility/EditingHUD.qml:120
msgid "Underexposed;Overexposed"
msgstr "Underexposed;Overexposed;cus deisearais;gainnead deisearais"

#: rc/qml/Utility/EditingHUD.qml:121 rc/qml/Utility/EditingHUD.qml:162
msgid "Confirm"
msgstr "Dearbhaich"

#: rc/qml/Utility/EditingHUD.qml:125
msgid "Compensation"
msgstr "Co-leasachadh"

#: rc/qml/Utility/EditingHUD.qml:159
msgid "Color Balance"
msgstr "Cothromachadh dhathan"

#: rc/qml/Utility/EditingHUD.qml:160
msgid "Adjust color balance"
msgstr "Cuir air gleus cothromachadh nan dathan"

#: rc/qml/Utility/EditingHUD.qml:161
msgid "Saturation;Hue"
msgstr "Saturation;Hue;sàthachd;tuar"

#: rc/qml/Utility/EditingHUD.qml:166
msgid "Brightness"
msgstr "Soilleireachd"

#: rc/qml/Utility/EditingHUD.qml:177
msgid "Contrast"
msgstr "Iomsgaradh"

#: rc/qml/Utility/EditingHUD.qml:188
msgid "Saturation"
msgstr "Sàthachd"

#: rc/qml/Utility/EditingHUD.qml:199
msgid "Hue"
msgstr "Tuar"

#: desktop/gallery-app.desktop.in.in.h:2
msgid "Ubuntu Photo Viewer"
msgstr "Sealladair dealbhan Ubuntu"

#: desktop/gallery-app.desktop.in.in.h:3
msgid "Browse your photographs"
msgstr "Rùraich tro na dealbhan agad"

#: desktop/gallery-app.desktop.in.in.h:4
msgid "Photographs;Pictures;Albums"
msgstr "Photographs;Pictures;Albums;Dealbhan;Dealbhan-camara;Albaman"

#~ msgid "Delete 1 photo"
#~ msgstr "Sguab às 1 dealbh"

#~ msgid "Delete 1 video"
#~ msgstr "Sguab às 1 video"

#~ msgid "Delete %1 photos and 1 video"
#~ msgstr "Sguab às %1 dealbh(an) is 1 video"

#~ msgid "Delete 1 photo and %1 videos"
#~ msgstr "Sguab às 1 dealbh is %1 video(than)"

#~ msgid "Delete 1 photo and 1 video"
#~ msgstr "Sguab às 1 video is 1 dealbh"

#~ msgid "Delete %1 photos and %2 videos"
#~ msgstr "Sguab às %1 dealbh(an) is %2 video(than)"
