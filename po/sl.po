# Slovenian translation for gallery-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the gallery-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: gallery-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-11-22 21:06+0000\n"
"PO-Revision-Date: 2017-06-28 18:00+0000\n"
"Last-Translator: Kristijan <kiki.tkalec@gmail.com>\n"
"Language-Team: Slovenian <https://ubpweblate.tnvcomp.com/projects/ubports/"
"gallery-app/sl/>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n"
"%100==4 ? 2 : 3;\n"
"X-Generator: Weblate 2.14.1\n"
"X-Launchpad-Export-Date: 2017-04-05 07:16+0000\n"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:43
#: rc/qml/AlbumEditor/AlbumEditor.qml:36
msgid "Edit album"
msgstr "Uredi album"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:50
#: rc/qml/AlbumViewer/AlbumViewer.qml:378 rc/qml/Components/DeleteDialog.qml:32
#: rc/qml/EventsOverview.qml:139 rc/qml/MediaViewer/MediaViewer.qml:350
#: rc/qml/PhotosOverview.qml:195 rc/qml/Utility/EditingHUD.qml:66
msgid "Delete"
msgstr "Izbriši"

#: rc/qml/AlbumEditor/AlbumEditor.qml:39 rc/qml/Components/MediaSelector.qml:93
#: rc/qml/EventsOverview.qml:161 rc/qml/MediaViewer/MediaViewer.qml:278
#: rc/qml/PhotosOverview.qml:218 rc/qml/PickerScreen.qml:285
msgid "Cancel"
msgstr "Prekliči"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:113
msgid "Default"
msgstr "Privzeto"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:114
msgid "Blue"
msgstr "Modra"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:115
msgid "Green"
msgstr "Zelena"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:116
msgid "Pattern"
msgstr "Vzorec"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:117
msgid "Red"
msgstr "Rdeča"

#: rc/qml/AlbumViewer/AlbumViewer.qml:107 rc/qml/MainScreen.qml:62
msgid "Album"
msgstr "Album"

#: rc/qml/AlbumViewer/AlbumViewer.qml:372
#: rc/qml/MediaViewer/MediaViewer.qml:340
msgid "Add to album"
msgstr "Dodaj v album"

#: rc/qml/AlbumsOverview.qml:229 rc/qml/Components/PopupAlbumPicker.qml:91
msgid "Add new album"
msgstr "Dodaj nov album"

#: rc/qml/AlbumsOverview.qml:233 rc/qml/Components/PopupAlbumPicker.qml:101
msgid "New Photo Album"
msgstr "Nov album s fotografijami"

#: rc/qml/AlbumsOverview.qml:234 rc/qml/Components/PopupAlbumPicker.qml:102
msgid "Subtitle"
msgstr "Podnapis"

#: rc/qml/AlbumsOverview.qml:241 rc/qml/EventsOverview.qml:116
#: rc/qml/PhotosOverview.qml:173
msgid "Camera"
msgstr "Fotoaparat"

#: rc/qml/Components/DeleteDialog.qml:36 rc/qml/MediaViewer/MediaViewer.qml:226
msgid "Yes"
msgstr "Da"

#: rc/qml/Components/DeleteDialog.qml:45 rc/qml/MediaViewer/MediaViewer.qml:237
msgid "No"
msgstr "Ne"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:51
msgid "Delete album"
msgstr "Izbriši album"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:60
#, fuzzy
msgid "Delete album AND contents"
msgstr "Izbriši album in vsebino"

#: rc/qml/Components/EventCard.qml:59
msgid "MMM yyyy"
msgstr "MMM yyyy"

#: rc/qml/Components/EventCard.qml:73
msgid "dd"
msgstr "dd"

#: rc/qml/Components/MediaSelector.qml:59
#: rc/qml/Components/MediaSelector.qml:81
msgid "Add to Album"
msgstr "Dodaj v album"

#: rc/qml/Components/PopupAlbumPicker.qml:162 rc/qml/Utility/EditingHUD.qml:77
msgid "Add Photo to Album"
msgstr "Dodaj fotografijo v album"

#: rc/qml/Components/UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "Ni mogoče deliti"

#: rc/qml/Components/UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "Ni možno deliti slik in videoposnetkov hkrati"

#: rc/qml/Components/UnableShareDialog.qml:30
msgid "Ok"
msgstr "V redu"

#: rc/qml/EventsOverview.qml:80 rc/qml/PhotosOverview.qml:87
#, fuzzy, qt-format
msgid "Delete %1 photo"
msgid_plural "Delete %1 photos"
msgstr[0] "Izbriši %1 fotografij"
msgstr[1] "Izbriši %1 fotografij"
msgstr[2] "Izbriši %1 fotografij"
msgstr[3] "Izbriši %1 fotografij"

#: rc/qml/EventsOverview.qml:86 rc/qml/PhotosOverview.qml:93
#, fuzzy, qt-format
msgid "Delete %1 video"
msgid_plural "Delete %1 videos"
msgstr[0] "Izbriši %1 videov"
msgstr[1] "Izbriši %1 videov"
msgstr[2] "Izbriši %1 videov"
msgstr[3] "Izbriši %1 videov"

#: rc/qml/EventsOverview.qml:92 rc/qml/PhotosOverview.qml:99
#, fuzzy, qt-format
msgid "Delete %1 media file"
msgid_plural "Delete %1 media files"
msgstr[0] "Izbriši %1 videov"
msgstr[1] "Izbriši %1 videov"
msgstr[2] "Izbriši %1 videov"
msgstr[3] "Izbriši %1 videov"

#: rc/qml/EventsOverview.qml:109 rc/qml/MainScreen.qml:199
#: rc/qml/MainScreen.qml:251 rc/qml/PhotosOverview.qml:166
#: rc/qml/PickerScreen.qml:192 rc/qml/PickerScreen.qml:239
msgid "Select"
msgstr "Izberi"

#: rc/qml/EventsOverview.qml:128 rc/qml/PhotosOverview.qml:185
#: rc/qml/Utility/EditingHUD.qml:76
msgid "Add"
msgstr "Dodaj"

#: rc/qml/EventsOverview.qml:146 rc/qml/MediaViewer/MediaViewer.qml:361
#: rc/qml/PhotosOverview.qml:202 rc/qml/Utility/EditingHUD.qml:71
msgid "Share"
msgstr "Deli"

#: rc/qml/EventsOverview.qml:195 rc/qml/MediaViewer/MediaViewer.qml:182
#: rc/qml/PhotosOverview.qml:241
msgid "Share to"
msgstr "Souporaba z"

#: rc/qml/LoadingScreen.qml:39
msgid "Loading…"
msgstr "Nalaganje ..."

#: rc/qml/MainScreen.qml:148
msgid "Albums"
msgstr "Albumi"

#: rc/qml/MainScreen.qml:162 rc/qml/MainScreen.qml:201
#: rc/qml/PickerScreen.qml:154
msgid "Events"
msgstr "Dogodki"

#: rc/qml/MainScreen.qml:210 rc/qml/MainScreen.qml:253
#: rc/qml/PickerScreen.qml:203
msgid "Photos"
msgstr "Fotografije"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a photo"
msgstr "Izbriši fotografijo"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a video"
msgstr "Izbriši video"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a photo from album"
msgstr "Odstrani fotografijo iz albuma"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a video from album"
msgstr "Odstrani video iz albuma"

#: rc/qml/MediaViewer/MediaViewer.qml:257
msgid "Remove from Album"
msgstr "Odstrani iz albuma"

#: rc/qml/MediaViewer/MediaViewer.qml:268
msgid "Remove from Album and Delete"
msgstr "Odstrani iz albuma in izbriši"

#: rc/qml/MediaViewer/MediaViewer.qml:315
msgid "Edit"
msgstr "Uredi"

#: rc/qml/MediaViewer/MediaViewer.qml:371
msgid "Info"
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:394
msgid "Informations"
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:397
msgid "Media type: "
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:398
#, fuzzy
msgid "photo"
msgstr "Fotografije"

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "video"
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:399
msgid "Media name: "
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:400
msgid "Date: "
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:401
msgid "Time: "
msgstr ""

#: rc/qml/MediaViewer/MediaViewer.qml:407
msgid "ok"
msgstr ""

#: rc/qml/MediaViewer/PhotoEditorPage.qml:26
msgid "Edit Photo"
msgstr "Uredi fotografijo"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:46
#: desktop/gallery-app.desktop.in.in.h:1
msgid "Gallery"
msgstr "Galerija"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:183
msgid "Toggle Selection"
msgstr "Preklopi izbiro"

#: rc/qml/MediaViewer/SingleMediaViewer.qml:241
msgid "An error has occurred attempting to load media"
msgstr ""

#: rc/qml/PhotosOverview.qml:116 rc/qml/PhotosOverview.qml:159
msgid "Grid Size"
msgstr ""

#: rc/qml/PhotosOverview.qml:117
msgid "Select the grid size in gu units between 8 and 20 (default is 12)"
msgstr ""

#: rc/qml/PhotosOverview.qml:132
msgid "Finished"
msgstr ""

#: rc/qml/PickerScreen.qml:291
msgid "Pick"
msgstr "Izberite"

#: rc/qml/Utility/EditingHUD.qml:67
msgid "Trash;Erase"
msgstr "Smeti;Izbriši"

#: rc/qml/Utility/EditingHUD.qml:72
msgid "Post;Upload;Attach"
msgstr "Objavi;Pošlji;Priloži"

#: rc/qml/Utility/EditingHUD.qml:81
msgid "Undo"
msgstr "Razveljavi"

#: rc/qml/Utility/EditingHUD.qml:82
msgid "Cancel Action;Backstep"
msgstr "Prekliči dejanje;Korak nazaj"

#: rc/qml/Utility/EditingHUD.qml:86
msgid "Redo"
msgstr "Uveljavi"

#: rc/qml/Utility/EditingHUD.qml:87
msgid "Reapply;Make Again"
msgstr "Ponovno uveljavi;Ponovno ustvari"

#: rc/qml/Utility/EditingHUD.qml:91
msgid "Auto Enhance"
msgstr "Samodejno izboljšaj"

#: rc/qml/Utility/EditingHUD.qml:92
msgid "Adjust the image automatically"
msgstr "Prilagodi sliko samodejno"

#: rc/qml/Utility/EditingHUD.qml:93
msgid "Automatically Adjust Photo"
msgstr "Samodejno prilagodi fotografijo"

#: rc/qml/Utility/EditingHUD.qml:98
msgid "Rotate"
msgstr "Zavrti"

#: rc/qml/Utility/EditingHUD.qml:99
msgid "Turn Clockwise"
msgstr "Obrni v smeri urinega kazalca"

#: rc/qml/Utility/EditingHUD.qml:100
msgid "Rotate the image clockwise"
msgstr "Zavrti sliko v smeri urinega kazalca"

#: rc/qml/Utility/EditingHUD.qml:105
msgid "Crop"
msgstr "Obreži"

#: rc/qml/Utility/EditingHUD.qml:106
msgid "Trim;Cut"
msgstr "Obreži;Izreži"

#: rc/qml/Utility/EditingHUD.qml:107
msgid "Crop the image"
msgstr "Obreži sliko"

#: rc/qml/Utility/EditingHUD.qml:112
msgid "Revert to Original"
msgstr "Povrni na izvirnik"

#: rc/qml/Utility/EditingHUD.qml:113
msgid "Discard Changes"
msgstr "Zavrzi spremembe"

#: rc/qml/Utility/EditingHUD.qml:114
msgid "Discard all changes"
msgstr "Zavrzi vse spremembe"

#: rc/qml/Utility/EditingHUD.qml:118
msgid "Exposure"
msgstr "Osvetlitev"

#: rc/qml/Utility/EditingHUD.qml:119
msgid "Adjust the exposure"
msgstr "Prilagodi osvetlitev"

#: rc/qml/Utility/EditingHUD.qml:120
msgid "Underexposed;Overexposed"
msgstr "Premalo osvetljeno;Preveč osvetljeno"

#: rc/qml/Utility/EditingHUD.qml:121 rc/qml/Utility/EditingHUD.qml:162
msgid "Confirm"
msgstr "Potrdi"

#: rc/qml/Utility/EditingHUD.qml:125
msgid "Compensation"
msgstr "Kompenzacija"

#: rc/qml/Utility/EditingHUD.qml:159
msgid "Color Balance"
msgstr "Barvna uravnoteženost"

#: rc/qml/Utility/EditingHUD.qml:160
msgid "Adjust color balance"
msgstr "Prilagodi barvno uravnoteženost"

#: rc/qml/Utility/EditingHUD.qml:161
msgid "Saturation;Hue"
msgstr "Nasičenost;Obarvanost"

#: rc/qml/Utility/EditingHUD.qml:166
msgid "Brightness"
msgstr "Svetlost"

#: rc/qml/Utility/EditingHUD.qml:177
msgid "Contrast"
msgstr "Kontrast"

#: rc/qml/Utility/EditingHUD.qml:188
msgid "Saturation"
msgstr "Nasičenost"

#: rc/qml/Utility/EditingHUD.qml:199
msgid "Hue"
msgstr "Obarvanost"

#: desktop/gallery-app.desktop.in.in.h:2
msgid "Ubuntu Photo Viewer"
msgstr "Ubuntu pregledovalnik fotografij"

#: desktop/gallery-app.desktop.in.in.h:3
msgid "Browse your photographs"
msgstr "Brskajte po svojih fotografijah"

#: desktop/gallery-app.desktop.in.in.h:4
msgid "Photographs;Pictures;Albums"
msgstr "Fotografije;Slike;Albumi"

#~ msgid "Delete 1 photo"
#~ msgstr "Izbriši 1 fotografijo"

#~ msgid "Delete 1 video"
#~ msgstr "Izbriši 1 videoposnetek"

#~ msgid "Delete %1 photos and 1 video"
#~ msgstr "Izbriši %1 fotografij in 1 video"

#~ msgid "Delete 1 photo and %1 videos"
#~ msgstr "Izbriši 1 fotografijo in %1 videov"

#~ msgid "Delete 1 photo and 1 video"
#~ msgstr "Izbriši 1 fotografijo in 1 video"

#~ msgid "Delete %1 photos and %2 videos"
#~ msgstr "Izbriši %1 fotografij in %2 videov"
